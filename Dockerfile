FROM python:3.7-stretch
MAINTAINER Josh Cox <josh 'at' webhosting coop>

ENV LANG=en_US.UTF-8 \
  LANGUAGE=en_US.UTF-8 \
  DEBIAN_FRONTEND=noninteractive

RUN \
apt-get -yqq update \
&& apt-get install -yqq sshfs ftp lftp inotify-tools \
&& mkdir /watch \
&& rm -rf /var/lib/apt/lists/*

# End non-interactive apt
ENV DEBIAN_FRONTEND interactive

WORKDIR /watch
RUN pip3 install watchdog

COPY assets /assets
#CMD ["/assets/watcher.py", "/watch"]
CMD ["/assets/watcher.bash", "/watch"]
