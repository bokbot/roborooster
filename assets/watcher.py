#!/usr/bin/env python3

import sys
import os
import time
import logging
import ftplib
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from watchdog.events import FileSystemEventHandler

ftp_user = os.getenv('FTP_USER', "watchdog")
ftp_pass = os.getenv('FTP_PASS', "watchdog123")
ftp_host = os.getenv('FTP_HOST', "127.0.0.1")

ftp = ftplib.FTP_TLS(ftp_host, ftp_user, ftp_pass)
ftp.set_pasv(os.getenv('FTP_PASV', 'true'))

class Handler(FileSystemEventHandler):

    @staticmethod
    def on_any_event(event):
        if event.is_directory:
            return None

        elif event.event_type == 'created':
            # Take any action here when a file is first created.
            print("Received created event - %s." % event.src_path)
            ftp_file = open(event.src_path, 'r')
            ftp.storbinary("STOR " + event.src_path, ftp_file)


        elif event.event_type == 'modified':
            # Taken any action here when a file is modified.
            print("Received modified event - %s." % event.src_path)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    path = sys.argv[1] if len(sys.argv) > 1 else '.'
    #event_handler = LoggingEventHandler()
    event_handler = Handler()
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
