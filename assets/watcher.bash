#!/bin/bash
find $1 -type f | xargs -n1 -I% lftp -p21 -u $FTP_USER,$FTP_PASS $FTP_HOST/ -e "put -E %; bye"
inotifywait -m $1 -e create -e moved_to |
  while read path action file; do
    echo "The file '$file' appeared in directory '$path' via '$action'"
    lftp -p21 -u $FTP_USER,$FTP_PASS $FTP_HOST/$FTP_PATH -e "put -E $path/$file; bye"
  done
